# Maintainer: Philip Müller <philm[at]manjaro[dot]org>
# Maintainer: Bernhard Landauer <bernhard[at]manjaro[dot]org>
# Maintainer: Helmut Stult <helmut[at]manjaro[dot]org>

# Arch credits:
# Maintainer: Lukas Fleischer <archlinux at cryptocrack dot de>
# Contributor: xduugu
# Contributor: nh2
# Contributor: Steven Davidovitz <steviedizzle ð gmail đ com>
# Contributor: Nick B <Shirakawasuna ð gmail đ com>
# Contributor: Christof Musik <christof ð senfdax đ de>
# Contributor: Stefan Rupp <archlinux ð stefanrupp đ de>
# Contributor: Ignas Anikevicius <anikevicius ð gmail đ com>

_linuxprefix=linux-xanmod-lts
_extraver=extramodules-6.1-xanmod
_pkgname=tp_smapi
_kernver="$(cat /usr/lib/modules/${_extraver}/version)"
_extramodules=$(readlink -f "/usr/lib/modules/${_kernver}/extramodules")
pkgname=$_linuxprefix-tp_smapi
_pkgver=0.43
pkgver=0.43_6.1.23.xanmod1_1
pkgrel=1
pkgdesc="Modules for ThinkPad's SMAPI functionality"
arch=('x86_64')
url='https://github.com/evgeni/tp_smapi'
license=('GPL')
makedepends=('git' "$_linuxprefix" "$_linuxprefix-headers")
groups=("$_linuxprefix-extramodules")
install="${_pkgname}.install"
_commit=a63729ab30d85430048f65c37f29188ab484cd52  # tags/tp-smapi/0.43
source=("git+https://github.com/evgeni/tp_smapi#commit=$_commit")
sha256sums=('SKIP')

pkgver() {
    cd $_pkgname
    _ver=$(pacman -Q $_linuxprefix | cut -d " " -f 2 | cut -f1-2 -d'-')
    _pkgver=$(git describe --tags | sed 's/^tp-smapi\///;s/-/+/g')
    printf '%s' "${_pkgver}_${_ver/-/_}"
}

build() {
  cd $_pkgname

  # https://bugs.archlinux.org/task/54975 (kernel has no _GLOBAL_OFFSET_TABLE_):
  # Clear EXTRA_CFLAGS since it defaults to injecting CFLAGS and -fno-plt breaks the modules
  make HDAPS=1 KVER="$(</usr/lib/modules/${_extraver}/version)" EXTRA_CFLAGS=
}

package() {
  _ver=$(pacman -Q $_linuxprefix | cut -d " " -f 2)
  depends=("${_linuxprefix}=${_ver}")
  replaces=("linux510-xanmod-${_pkgname}")

  # install kernel modules
  find . -name "*.ko" -exec install -Dt "$pkgdir$_extramodules" {} +

  # compress kernel modules
  find "${pkgdir}" -name "*.ko" -exec gzip -9 {} +

  # load module on startup
  echo tp_smapi > "${srcdir}/${_pkgname}.conf"
  install -Dm644 "${srcdir}/${_pkgname}.conf" "${pkgdir}/usr/lib/modules-load.d/${pkgname}.conf"

  # update kernel version in install file
  sed -ri "s#^(extramodules=).*\$#\1${_extramodules}#" "${startdir}/${_pkgname}.install"
}
